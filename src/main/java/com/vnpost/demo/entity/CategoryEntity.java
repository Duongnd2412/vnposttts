package com.vnpost.demo.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@Entity
@Table(schema = "dbo", name = "category")
@Getter
@Setter
public class CategoryEntity extends BaseEntity {
    @Column(name = "name")
    private String name;

    @OneToMany(mappedBy = "categoryId")
    List<NewEntity> newEntities = new ArrayList<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "group_id")
    private GroupCategoryEntity groupCategory;


}
