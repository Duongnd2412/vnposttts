package com.vnpost.demo.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(schema = "dbo",name = "news")
@Getter
@Setter
public class NewEntity extends BaseEntity{
    @Column(name = "title")
    private String title;
    @Column(name = "thumbnail")
    private String thumbnail;
    @Column(name = "shortdescription")
    private String shortDescription;
    @Column(name = "content")
    private String content;

    @Column(name = "count")
    private Integer count;

    @ManyToOne
    @JoinColumn(name = "category_id")
    private CategoryEntity categoryId;
}
