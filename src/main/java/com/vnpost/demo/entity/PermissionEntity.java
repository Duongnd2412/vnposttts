package com.vnpost.demo.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(schema = "dbo",name = "permission")
public class PermissionEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "name")
    private String name;
    @Column(name = "code")
    private String code;

    @ManyToMany(mappedBy = "permissionEntities")
    private List<GroupPmEntity> groupPmEntities = new ArrayList<>();

    public List<GroupPmEntity> getGroupPmEntities() {
        return groupPmEntities;
    }

    public void setGroupPmEntities(List<GroupPmEntity> groupPmEntities) {
        this.groupPmEntities = groupPmEntities;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
