package com.vnpost.demo.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(schema = "dbo", name = "group_category")
@Getter
@Setter
public class GroupCategoryEntity extends BaseEntity {
    @Column(name = "name")
    private String name;

    @OneToMany(mappedBy = "groupCategory")
    List<CategoryEntity> categoryEntities = new ArrayList<>();

}
