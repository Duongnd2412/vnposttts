package com.vnpost.demo.util;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class EncrytPasswordUtils {
    public static String encrytePassword(String password) {
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        return encoder.encode(password);
    }
}
