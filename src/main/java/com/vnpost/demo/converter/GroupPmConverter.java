package com.vnpost.demo.converter;

import com.vnpost.demo.dto.GroupPmDto;
import com.vnpost.demo.entity.GroupPmEntity;
import com.vnpost.demo.entity.RoleEntity;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class GroupPmConverter {

    public GroupPmDto toDto(Optional<GroupPmEntity> groupPmEntity){
        GroupPmDto groupPmDto = new GroupPmDto();
        groupPmDto.setId(groupPmEntity.get().getId());
        groupPmDto.setPermissionId(groupPmEntity.get().getPermissionEntities()
                .stream()
                .map(permissionEntity -> permissionEntity.getId())
                .collect(Collectors.toList()));

        groupPmDto.setName(groupPmEntity.get().getName());
        groupPmDto.setCode(groupPmEntity.get().getCode());
        return groupPmDto;
    }
}
