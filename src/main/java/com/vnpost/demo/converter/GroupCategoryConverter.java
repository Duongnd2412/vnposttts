package com.vnpost.demo.converter;

import com.vnpost.demo.dto.GroupCategoryDto;
import com.vnpost.demo.entity.GroupCategoryEntity;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public class GroupCategoryConverter {

    public GroupCategoryDto toDto(GroupCategoryEntity entity){
        GroupCategoryDto dto = new GroupCategoryDto();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setIds(entity.getCategoryEntities().stream().map(categoryEntity -> categoryEntity.getId()).collect(Collectors.toList()));
        dto.setCategoryName(entity.getCategoryEntities().stream().map(categoryEntity -> categoryEntity.getName()).collect(Collectors.toList()));
        return dto;
    }
}
