package com.vnpost.demo.converter;

import com.vnpost.demo.dto.NewDto;
import com.vnpost.demo.entity.NewEntity;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class NewConverter {
    @Autowired
    private ModelMapper modelMapper;

    public NewDto toDto(NewEntity newEntity){
        NewDto newDto = modelMapper.map(newEntity,NewDto.class);
        newDto.setCategoryId(newEntity.getCategoryId().getId());
        return newDto;
    }
}
