package com.vnpost.demo.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class GroupCategoryDto extends AbstractDto {
    private String name;
    private List<NewDto> listNew = new ArrayList<>();
    private List<String> categoryName = new ArrayList<>();
    private List<Long> ids = new ArrayList<>();
}
