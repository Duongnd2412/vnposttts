package com.vnpost.demo.dto;

import java.util.ArrayList;
import java.util.List;


public class RoleDto extends AbstractDto{
    private String name;
    private String code;
    List<Long> groupId = new ArrayList<>();
    List<Long> userId = new ArrayList<>();





    public List<Long> getUserId() {
        return userId;
    }

    public void setUserId(List<Long> userId) {
        this.userId = userId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Long> getGroupId() {
        return groupId;
    }

    public void setGroupId(List<Long> groupId) {
        this.groupId = groupId;
    }
}
