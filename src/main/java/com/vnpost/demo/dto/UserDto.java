package com.vnpost.demo.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserDto extends AbstractDto{
    private String email;
    private String password;
    private String confirmPassword;
    private String oldPassword;
    private String phone;
    private String gender;
    private String avatar;
    private String fullName;
    private String roleCode;
    private Long roleId;
    private String roleName;
    private String address;
    private int checkLogin;


}
