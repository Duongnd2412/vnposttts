package com.vnpost.demo.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class GroupPmDto extends AbstractDto {
    private String name;
    private String code;
    private List<Long> idRole = new ArrayList<>();
    private List<Long> permissionId = new ArrayList<>();
    private List<PermissionDto> listPermission = new ArrayList<>();
    private int check;

}
