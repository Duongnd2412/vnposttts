package com.vnpost.demo.dto;




import java.util.ArrayList;
import java.util.List;


public class PermissionDto extends AbstractDto {
    private String name;
    private String code;
    private List<Long> groupId = new ArrayList<>();

    private int checked;

    public int getChecked() {
        return checked;
    }

    public void setChecked(int checked) {
        this.checked = checked;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Long> getGroupId() {
        return groupId;
    }

    public void setGroupId(List<Long> groupId) {
        this.groupId = groupId;
    }
}
