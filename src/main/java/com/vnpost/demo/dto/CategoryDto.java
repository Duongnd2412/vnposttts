package com.vnpost.demo.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Getter
@Setter
public class CategoryDto extends AbstractDto {
   private String name;
   private Long groupId;
   private String groupName;
   List<Long> ids = new ArrayList<>();
   List<NewDto> listNew = new ArrayList<>();
}
