package com.vnpost.demo.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NewDto extends AbstractDto {
    private String title;
    private String thumbnail;
    private String shortDescription;
    private String content;
    private String categoryName;
    private Long categoryId;
    private Integer count;

}
