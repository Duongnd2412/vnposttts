package com.vnpost.demo.controller.web;

import com.vnpost.demo.dto.MyUser;
import com.vnpost.demo.sevice.RoleService;
import com.vnpost.demo.util.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller(value = "web")
public class HomeController {


    @GetMapping({"/trang-chu","/"})
    public ModelAndView homePage(){
        ModelAndView mav = new ModelAndView("web/home");

        return mav;
    }
    @GetMapping("/dang-nhap")
    public ModelAndView login(){
        ModelAndView mav = new ModelAndView("login");
        return mav;
    }


}
