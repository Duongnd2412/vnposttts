package com.vnpost.demo.controller.web;

import com.vnpost.demo.dto.NewDto;
import com.vnpost.demo.sevice.CategoryService;
import com.vnpost.demo.sevice.GroupCategoryService;
import com.vnpost.demo.sevice.NewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.ModelAndView;

@Controller(value = "webNew")
public class NewController {
    @Autowired
    private NewService newService;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private GroupCategoryService groupService;

    @GetMapping("/tin-tuc")
    public ModelAndView newPage(){
        ModelAndView mav = new ModelAndView("/web/news/index");
        mav.addObject("group",groupService.findAllNew());
        mav.addObject("category",categoryService.findAll());
        return mav;
    }

    @GetMapping("/bai-viet/chi-tiet/{id}")
    public String getNew(Model model, @PathVariable("id")Long id){
        NewDto newDto = newService.findById(id);
        model.addAttribute("group",groupService.findAllNew());
        model.addAttribute("category",categoryService.findAll());
        model.addAttribute("news",newDto);
            return "web/news/new";

    }
}
