package com.vnpost.demo.controller.admin;

import com.vnpost.demo.util.SecurityUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller(value = "admin")
public class HomeController {

    @GetMapping("/admin")
    public ModelAndView homePage(){
        ModelAndView mav = new ModelAndView("admin/home");
        mav.addObject("roles", SecurityUtils.getPrincipal().getAuthorities());
        return mav;
    }
}
