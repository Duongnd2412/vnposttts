package com.vnpost.demo.controller.admin;

import com.vnpost.demo.dto.GroupPmDto;
import com.vnpost.demo.dto.PermissionDto;
import com.vnpost.demo.dto.RoleDto;
import com.vnpost.demo.dto.UserDto;
import com.vnpost.demo.sevice.GroupPmService;
import com.vnpost.demo.sevice.PermissionService;
import com.vnpost.demo.sevice.RoleService;
import com.vnpost.demo.sevice.UserService;
import com.vnpost.demo.sevice.impl.CustomUserDetailsService;
import com.vnpost.demo.util.MessageUtil;
import com.vnpost.demo.util.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;


import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;


@Controller(value = "adminUser")
@RequestMapping("/admin/user")
public class UserController {

    @Autowired
    private UserService userService;
    @Autowired
    private PermissionService permission;
    @Autowired
    private RoleService roleService;
    @Autowired
    private MessageUtil messageUtil;
    @Autowired
    private GroupPmService groupPmService;

    @GetMapping("/change")
    public ModelAndView changeInfo(){
        ModelAndView mav = new ModelAndView("web/user/changeInfo");
        mav.addObject("viewmodel",new UserDto());
        return mav;
    }

    @PostMapping("/changePassword")
    public RedirectView changePassword(@ModelAttribute("viewmodel") UserDto userDTO){
        userService.changePass(userDTO);
        return new RedirectView("/admin");
    }

    @GetMapping("/list")
    public ModelAndView showList(HttpServletRequest request){
        ModelAndView mav = new ModelAndView("admin/user/list");
        if (request.getParameter("message") != null){
            Map<String , String> message = messageUtil.getMessage(request.getParameter("message"));
            mav.addObject("message",message.get("message"));
            mav.addObject("alert",message.get("alert"));
        }
        mav.addObject("users",userService.findAll());
        mav.addObject("roles", SecurityUtils.getPrincipal().getAuthorities());
        return mav;
    }
    @GetMapping("/role")
    public ModelAndView listRole(HttpServletRequest request){
        ModelAndView mav = new ModelAndView("admin/user/listRole");
        if (request.getParameter("message") != null){
            Map<String , String>message = messageUtil.getMessage(request.getParameter("message"));
            mav.addObject("message",message.get("message"));
            mav.addObject("alert",message.get("alert"));
        }
        mav.addObject("role",roleService.findAll());
        mav.addObject("roles", SecurityUtils.getPrincipal().getAuthorities());
        return mav;
    }
    @GetMapping("/edit-user/{id}")
    public ModelAndView editNew(@PathVariable("id")long id,HttpServletRequest request){
        ModelAndView mav = new ModelAndView("admin/user/edit");
        UserDto userDto = userService.findById(id);
        if (request.getParameter("message") != null){
            Map<String , String> message = messageUtil.getMessage(request.getParameter("message"));
            mav.addObject("message",message.get("message"));
            mav.addObject("alert",message.get("alert"));
        }
        mav.addObject("role",roleService.findAll());
        mav.addObject("editUser",userDto);
        mav.addObject("roles", SecurityUtils.getPrincipal().getAuthorities());
        return mav;
    }

    @GetMapping("permission/{id}")
    public ModelAndView editPermission(@PathVariable("id")long id,HttpServletRequest request){
        ModelAndView mav = new ModelAndView("admin/user/editPermission");
        GroupPmDto groupPmDto = groupPmService.findById(id);
        if (request.getParameter("message") != null){
            Map<String , String> message = messageUtil.getMessage(request.getParameter("message"));
            mav.addObject("message",message.get("message"));
            mav.addObject("alert",message.get("alert"));
        }
        List<PermissionDto> permissionDtos = permission.findAll();
        for (Long ids: groupPmDto.getPermissionId()){
            for (PermissionDto permissionDto: permissionDtos){
                if (ids == permissionDto.getId()){
                    permissionDto.setChecked(1);
                }
            }
        }
        mav.addObject("groups",groupPmDto);
        mav.addObject("permissions",permissionDtos);
        mav.addObject("roles", SecurityUtils.getPrincipal().getAuthorities());
        return mav;
    }
    @GetMapping("/role/{id}")
    public ModelAndView editRole(@PathVariable("id")long id, HttpServletRequest request){
        ModelAndView mav =new ModelAndView("admin/user/editRole");
        RoleDto roleDto = roleService.findById(id);
        if (request.getParameter("message") != null){
            Map<String , String> message = messageUtil.getMessage(request.getParameter("message"));
            mav.addObject("message",message.get("message"));
            mav.addObject("alert",message.get("alert"));
        }
        List<GroupPmDto> groupPmDtos = groupPmService.findAll();
        for (Long ids : roleDto.getGroupId()){
            for (GroupPmDto idGroup : groupPmDtos){
                if(ids == idGroup.getId()){
                    idGroup.setCheck(1);
                }
            }
        }

        mav.addObject("editRole",roleDto);
        mav.addObject("permissionsGr",groupPmDtos);
        mav.addObject("roles", SecurityUtils.getPrincipal().getAuthorities());
        return mav;
    }

//    @GetMapping("/delete/{id}")
//    public String delete(@PathVariable("id") List<Long> id){
//        userService.delete(id);
//        return "admin/user/list" ;
//    }

    @GetMapping("/create-user")
    public ModelAndView createUser(){
        ModelAndView mav = new ModelAndView("admin/user/create");
        mav.addObject("role",roleService.findAll());
        mav.addObject("roles", SecurityUtils.getPrincipal().getAuthorities());
        return mav;
    }

}
