package com.vnpost.demo.controller.admin;

import com.vnpost.demo.dto.AbstractDto;
import com.vnpost.demo.dto.CategoryDto;

import com.vnpost.demo.sevice.CategoryService;

import com.vnpost.demo.sevice.GroupCategoryService;
import com.vnpost.demo.util.MessageUtil;

import com.vnpost.demo.util.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

@Controller("adminCategory")
@RequestMapping("/admin/category")
public class CategoryController {
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private MessageUtil messageUtil;
    @Autowired
    private GroupCategoryService groupService;


    @GetMapping("/list")
    public ModelAndView showList(HttpServletRequest request,
                                 @RequestParam("page") int page,
                                 @RequestParam("limit") int limit){
        AbstractDto abstractDto = new CategoryDto();
        abstractDto.setPage(page);
        abstractDto.setLimit(limit);
        abstractDto.setTotalItem(categoryService.getTotalItem());
        abstractDto.setTotalPage((int)Math.ceil((double) abstractDto.getTotalItem()/limit));
        Sort sort = Sort.by(Sort.Order.desc("id"));
        ModelAndView mav = new ModelAndView("admin/category/list");
        Pageable pageable = PageRequest.of(page-1,limit,sort);
        if (request.getParameter("message") != null){
            Map<String , String> message = messageUtil.getMessage(request.getParameter("message"));
            mav.addObject("message",message.get("message"));
            mav.addObject("alert",message.get("alert"));
        }
        mav.addObject("model",abstractDto);
        mav.addObject("category",categoryService.findAll(pageable));
        mav.addObject("roles", SecurityUtils.getPrincipal().getAuthorities());
        return mav;
    }

    @GetMapping("/create-category")
    public ModelAndView create(){
        ModelAndView mav = new ModelAndView("admin/category/create");
        mav.addObject("groups",groupService.findAllNew());
        mav.addObject("roles", SecurityUtils.getPrincipal().getAuthorities());
        return mav;
    }
    @GetMapping("/edit-category/{id}")
    public String edit(@PathVariable(value = "id",required = false)Long id, Model model,HttpServletRequest request){
        CategoryDto categoryDto = categoryService.findById(id);
        if (request.getParameter("message") != null){
            Map<String , String> message = messageUtil.getMessage(request.getParameter("message"));
            model.addAttribute("message",message.get("message"));
            model.addAttribute("alert",message.get("alert"));
        }

        model.addAttribute("groups",groupService.findAllNew());
        model.addAttribute("categories",categoryDto);
        model.addAttribute("roles", SecurityUtils.getPrincipal().getAuthorities());
        return "admin/category/edit";
    }



}
