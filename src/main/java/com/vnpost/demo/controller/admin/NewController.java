package com.vnpost.demo.controller.admin;


import com.vnpost.demo.dto.AbstractDto;
import com.vnpost.demo.dto.NewDto;

import com.vnpost.demo.sevice.CategoryService;
import com.vnpost.demo.sevice.NewService;
import com.vnpost.demo.util.MessageUtil;
import com.vnpost.demo.util.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;


@Controller(value = "adminNew")
@RequestMapping("/admin/new")
public class NewController {

    @Autowired
    private NewService newService;

    @Autowired
    private CategoryService categoryService;
    @Autowired
    private MessageUtil messageUtil;

    @GetMapping("/list")
    public ModelAndView showList(HttpServletRequest request,
                                 @RequestParam("page") int page,
                                 @RequestParam("limit") int limit){
        AbstractDto abstractDto = new NewDto();
        abstractDto.setPage(page);
        abstractDto.setLimit(limit);
        abstractDto.setTotalItem(newService.getTotalItem());
        abstractDto.setTotalPage((int)Math.ceil((double) abstractDto.getTotalItem()/limit));
        Sort sort = Sort.by(Sort.Order.desc("id"));
        ModelAndView mav= new ModelAndView("admin/new/list");
        Pageable pageable = PageRequest.of(page-1,limit,sort);
        if (request.getParameter("message") != null){
            Map<String , String> message = messageUtil.getMessage(request.getParameter("message"));
            mav.addObject("message",message.get("message"));
            mav.addObject("alert",message.get("alert"));
        }
        mav.addObject("model",abstractDto);
        mav.addObject("newDto",newService.findAll(pageable));
        mav.addObject("roles", SecurityUtils.getPrincipal().getAuthorities());
        return mav;
    }


    @GetMapping("/create-new")
    public ModelAndView createNew(){
        ModelAndView mav = new ModelAndView("admin/new/create");
        mav.addObject("category",categoryService.findAll());
        mav.addObject("roles", SecurityUtils.getPrincipal().getAuthorities());
        return mav;
    }
    @PostMapping("/create")
    public ModelAndView create(NewDto newDto){
        ModelAndView mav = new ModelAndView("admin/home");
        mav.addObject("createNew",newService.create(newDto));
        return mav;
    }
    @GetMapping("/edit-new/{id}")
    public String editNew(@PathVariable(value = "id",required = false)Long id, Model model,HttpServletRequest request){
        NewDto newDto = newService.findById(id);
        if (request.getParameter("message") != null){
            Map<String , String> message = messageUtil.getMessage(request.getParameter("message"));
            model.addAttribute("message",message.get("message"));
            model.addAttribute("alert",message.get("alert"));
        }
        model.addAttribute("category",categoryService.findAll());
        model.addAttribute("editNew",newDto);
        model.addAttribute("roles", SecurityUtils.getPrincipal().getAuthorities());
        return "admin/new/edit";
    }



}
