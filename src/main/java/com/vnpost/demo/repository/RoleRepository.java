package com.vnpost.demo.repository;

import com.vnpost.demo.entity.RoleEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<RoleEntity, Long> {
    RoleEntity findOneById(Long id);

    Optional<RoleEntity> findById(RoleEntity id);
    RoleEntity findOneByName(String name);


}
