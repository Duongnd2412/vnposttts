package com.vnpost.demo.repository.specification;

import com.vnpost.demo.dto.NewDto;

import com.vnpost.demo.entity.NewEntity;
import org.springframework.data.jpa.domain.Specification;

public class NewSpecification {

    public static Specification<NewEntity> filter(NewDto newDto){
        return Specification.where(withTitle(newDto.getTitle()))
                .and(withCategoryName(newDto.getCategoryName()))
                .and(withCategoryId(newDto.getCategoryId()));
    }

    private static Specification<NewEntity> withTitle(String title){
        if (title == null) return null;
        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.like(root.get("name"), title);
    }
    private static Specification<NewEntity> withCategoryName(String categoryName){
        if (categoryName == null)  return null;
        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(root.get("categoryName"),categoryName);
    }
    private static Specification<NewEntity> withCategoryId(Long categoryId){
        if (categoryId == null) return null;
        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(root.get("categoryId"),categoryId);
    }
}
