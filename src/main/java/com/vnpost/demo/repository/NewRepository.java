package com.vnpost.demo.repository;

import com.vnpost.demo.entity.CategoryEntity;
import com.vnpost.demo.entity.NewEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface NewRepository extends JpaRepository<NewEntity, Long>, JpaSpecificationExecutor<NewEntity> {
    List<NewEntity> findAllByCategoryId(Long id);

}
