package com.vnpost.demo.repository;

import com.vnpost.demo.entity.GroupCategoryEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GroupCategoryRepository extends JpaRepository<GroupCategoryEntity, Long> {
}
