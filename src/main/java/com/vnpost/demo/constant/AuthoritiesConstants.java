package com.vnpost.demo.constant;

public class AuthoritiesConstants {
    public static final String SUPER = "SUPER";
    public static final String SYS = "SYS";
    public static final String SP1 = "SP1";
    public static final String SP2 = "SP2";
    public static final String SP3 = "SP3";
    public static final String SP4 = "SP4";
    public static final String BCCP = "BCCP";
    public static final String TCBC = "TCBC";
    public static final String PPTT = "PPTT";
    public static final String TT = "TT";
    public static final String ADMIN = "ADMIN";
}
