package com.vnpost.demo.api;

import com.vnpost.demo.dto.GroupPmDto;
import com.vnpost.demo.dto.RoleDto;
import com.vnpost.demo.dto.UserDto;
import com.vnpost.demo.sevice.GroupPmService;
import com.vnpost.demo.sevice.RoleService;
import com.vnpost.demo.sevice.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/admin/api/user")
public class UserApi {
    @Autowired
    private  UserService userService;
    @Autowired
    private RoleService roleService;
    @Autowired
    private GroupPmService groupPmService;


    @PutMapping("/create")
    public UserDto create(@RequestBody UserDto userDto){
        return userService.create(userDto);
    }

    @PutMapping("/edit")
    public UserDto update(@RequestBody UserDto userDto){
        return userService.update(userDto);
    }


    @PutMapping("/role/{id}")
    public RoleDto updateRole(@RequestBody RoleDto roleDto,@PathVariable("id")long id){
        roleDto.setId(id);
        return roleService.updatePermissionGr(roleDto);
    }
    @DeleteMapping("/delete")
    public void deleteUser(@RequestBody long[] id){
        userService.delete(id);
    }

    @PutMapping("permission/{id}")
    public GroupPmDto updatePm(@RequestBody GroupPmDto groupPmDto,@PathVariable("id")long id){
        groupPmDto.setId(id);
        return groupPmService.updatePermission(groupPmDto);
    }
}

