package com.vnpost.demo.api;

import com.vnpost.demo.dto.GroupCategoryDto;
import com.vnpost.demo.dto.NewDto;
import com.vnpost.demo.dto.RoleDto;

import com.vnpost.demo.entity.GroupCategoryEntity;
import com.vnpost.demo.sevice.GroupCategoryService;
import com.vnpost.demo.sevice.NewService;
import com.vnpost.demo.sevice.PermissionService;
import com.vnpost.demo.sevice.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/api")
public class Test {
    @Autowired
    private PermissionService permissionService;
    @Autowired
    private RoleService roleService;
    @Autowired
    private NewService newService;
    @Autowired
    private GroupCategoryService categoryService;

    @PutMapping("/role/{id}")
    public RoleDto updateRole(@PathVariable("id")Long id,@RequestBody RoleDto roleDto){
        roleDto.setId(id);
        return roleService.updatePermissionGr(roleDto);
    }
    @GetMapping("/new/{id}")
    private NewDto news(@PathVariable("id")Long id){
        List<NewDto> newDtos = newService.findAllByCategory(id);
        return (NewDto) newDtos;
    }
    @GetMapping("/group/{id}")
    ResponseEntity <GroupCategoryDto> get(@PathVariable("id")Long id){
        GroupCategoryDto groupCategoryDto = categoryService.findAllNewById(id);
        return new ResponseEntity<>(groupCategoryDto,HttpStatus.OK)  ;
    }

}
