package com.vnpost.demo.api;

import com.vnpost.demo.dto.CategoryDto;
import com.vnpost.demo.dto.GroupCategoryDto;
import com.vnpost.demo.sevice.CategoryService;
import com.vnpost.demo.sevice.GroupCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/admin/api/category")
public class CategoryApi {
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private GroupCategoryService groupService;

    @GetMapping("/list")
    public ResponseEntity<List<CategoryDto>> getAll(){
        return new ResponseEntity<>(categoryService.findAll(), HttpStatus.OK);
    }

    @PutMapping("/create")
    public CategoryDto create(@RequestBody CategoryDto categoryDto){
        return categoryService.create(categoryDto);
    }

    @PutMapping("/create-group")
    public GroupCategoryDto create(@RequestBody GroupCategoryDto groupCategoryDto){
        return groupService.create(groupCategoryDto);
    }
    @PutMapping("/edit")
    public CategoryDto update(@RequestBody CategoryDto categoryDto){
        return categoryService.update(categoryDto);
    }

    @DeleteMapping("/delete")
    public void delete(@RequestBody long[] ids){
        categoryService.delete(ids);
    }

    @DeleteMapping("delete-group")
    public void deleteGroup(@RequestBody long[] id){
        groupService.delete(id);
    }

    @PutMapping("/edit-group")
    public GroupCategoryDto update(@RequestBody GroupCategoryDto groupCategoryDto){
        return groupService.update(groupCategoryDto);
    }

}
