package com.vnpost.demo.api;

import com.vnpost.demo.dto.CategoryDto;
import com.vnpost.demo.dto.NewDto;
import com.vnpost.demo.dto.UserDto;
import com.vnpost.demo.sevice.CategoryService;
import com.vnpost.demo.sevice.NewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/admin/api/new")
public class NewApi {

    @Autowired
    private NewService newService;
    @Autowired
    private CategoryService categoryService;

    @PutMapping("/create")
    public NewDto create(@RequestBody NewDto newDto){
       return newService.create(newDto);
    }

    @PutMapping("/edit")
    public NewDto update(@RequestBody NewDto newDto){
        return newService.update(newDto);
    }

    @DeleteMapping("/delete")
    public void delete(@RequestBody long[] id){
        newService.delete(id);
    }
    @GetMapping("/get/{id}")
    public ResponseEntity<NewDto> get(@PathVariable("id")long id){
        NewDto newDto = newService.findById(id);
        if(newDto==null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(newDto, HttpStatus.OK);
        }
    }
    @GetMapping("/category")
    public ResponseEntity<List<CategoryDto>>getall(){
        return new ResponseEntity<>(categoryService.findAll(),HttpStatus.OK);
    }

}
