package com.vnpost.demo.sevice.impl;
import com.vnpost.demo.converter.NewConverter;
import com.vnpost.demo.dto.NewDto;
import com.vnpost.demo.entity.CategoryEntity;
import com.vnpost.demo.entity.NewEntity;
import com.vnpost.demo.repository.CategoryRepository;
import com.vnpost.demo.repository.NewRepository;
import com.vnpost.demo.repository.specification.NewSpecification;
import com.vnpost.demo.sevice.NewService;
import com.vnpost.demo.util.CopyUtil;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class NewServiceImpl implements NewService {

    @Autowired
    private NewRepository newRepository;
    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private NewConverter converter;


    @Override
    public NewDto findById(Long id) {
        Optional<NewEntity> newEntity = newRepository.findById(id);
        return newEntity
                .map(newEntity1 ->modelMapper.map(newEntity1, NewDto.class))
                .orElse(null);
    }

    @Override
    public NewDto update(NewDto newDto) {
        NewEntity odlNew = newRepository.findById(newDto.getId()).get();
        NewEntity newNews = modelMapper.map(newDto,NewEntity.class);
        CopyUtil.copyOldToNewModel(odlNew,newNews);
        newNews.setCreatedBy(odlNew.getCreatedBy());
        newNews.setCreatedDate(odlNew.getCreatedDate());
        CategoryEntity categoryEntity =categoryRepository.findOneByName(newDto.getCategoryName());
        newNews.setCategoryId(categoryEntity);
        newRepository.save(newNews);
        return newDto;
    }

    @Override
    public NewDto create(NewDto newDto) {
        NewEntity newEntity = modelMapper.map(newDto, NewEntity.class);
        CategoryEntity categoryEntity =categoryRepository.findOneByName(newDto.getCategoryName());
        newEntity.setCategoryId(categoryEntity);
        newRepository.save(newEntity);
        return newDto;
    }

    @Override
    public void delete(long[] ids) {
        for (Long id: ids) {
            newRepository.deleteById(id);
        }
    }
    @Override
    public void countViews(Long id) {
        NewDto news = findById(id);
        if (news.getCount()==0 || news.getCount()==null){
            news.setCount(1);
        }else {
            news.setCount(news.getCount()+1);
        }
        NewEntity entity = modelMapper.map(news,NewEntity.class);
        entity.setCreatedDate(news.getCreatedDate());
        entity.setCreatedBy(news.getCreatedBy());
        newRepository.save(entity);
    }



    @Override
    public List<NewDto> findAll(Pageable pageable) {
        List<NewEntity> newEntities = newRepository.findAll(pageable).getContent();
        return newEntities
                .stream()
                .map(newEntity -> modelMapper.map(newEntity,NewDto.class))
                .collect(Collectors.toList());
    }

    @Override
    public int getTotalItem() {
        return (int) newRepository.count();
    }

    @Override
    public List<NewDto> findAll(NewDto newDto) {
        List<NewEntity> newEntities = newRepository.findAll(NewSpecification.filter(newDto));
        return newEntities
                .stream()
                .map(newEntity -> modelMapper.map(newEntity,NewDto.class))
                .collect(Collectors.toList());
    }



    @Override
    public List<NewDto> findAllByCategory(Long id) {
        List<NewEntity> newEntities = newRepository.findAllByCategoryId(id);
        return newEntities.stream().map(item -> modelMapper.map(item,NewDto.class)).collect(Collectors.toList());
    }


}
