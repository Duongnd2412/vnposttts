package com.vnpost.demo.sevice.impl;


import com.vnpost.demo.dto.MyUser;
import com.vnpost.demo.entity.GroupPmEntity;
import com.vnpost.demo.entity.PermissionEntity;
import com.vnpost.demo.entity.RoleEntity;
import com.vnpost.demo.entity.UserEntity;
import com.vnpost.demo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CustomUserDetailsService implements UserDetailsService {
    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserEntity userEntity = userRepository.findOneByEmail(username);

        if (userEntity == null){
            throw new UsernameNotFoundException("user not found");
        }
        List<GrantedAuthority> authorities = new ArrayList<>();
        RoleEntity roles = userEntity.getRoleId();
        authorities.add(new SimpleGrantedAuthority(roles.getCode()));
        for (GroupPmEntity groupPmEntity : roles.getGroupPmEntities()){
            authorities.add(new SimpleGrantedAuthority(groupPmEntity.getCode()));
            for (PermissionEntity permissionEntity : groupPmEntity.getPermissionEntities()){
                authorities.add(new SimpleGrantedAuthority(permissionEntity.getCode()));
            }
        }
        MyUser myUser = new MyUser(userEntity.getEmail()
                            ,userEntity.getPassword()
                ,true,true,true,true,authorities);
        myUser.setFullName(userEntity.getFullName());
        return myUser;
    }
}
