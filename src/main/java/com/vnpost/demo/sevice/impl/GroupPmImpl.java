package com.vnpost.demo.sevice.impl;

import com.vnpost.demo.converter.GroupPmConverter;
import com.vnpost.demo.dto.GroupPmDto;
import com.vnpost.demo.dto.NewDto;
import com.vnpost.demo.dto.PermissionDto;
import com.vnpost.demo.dto.RoleDto;
import com.vnpost.demo.entity.GroupPmEntity;
import com.vnpost.demo.entity.NewEntity;
import com.vnpost.demo.entity.PermissionEntity;
import com.vnpost.demo.entity.RoleEntity;
import com.vnpost.demo.repository.GroupPmRepository;
import com.vnpost.demo.repository.PermissionRepository;
import com.vnpost.demo.sevice.GroupPmService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class GroupPmImpl implements GroupPmService {
    @Autowired
    private GroupPmRepository repository;
    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private GroupPmConverter converter;
    @Autowired
    private PermissionRepository permissionRepository;


    @Override
    public List<GroupPmDto> findAll() {
        List<GroupPmEntity> groupPmEntities = repository.findAll();
        List<GroupPmDto> groupPmDtoList = new ArrayList<>();
        for (GroupPmEntity groupPmEntity : groupPmEntities){
            List<PermissionDto> permissionDtoList = new ArrayList<>();
            GroupPmDto groupPmDto = new GroupPmDto();
            List<PermissionDto> permissionDtos = groupPmEntity.getPermissionEntities().stream()
                                                .map(item -> modelMapper.map(item,PermissionDto.class)).collect(Collectors.toList());
            permissionDtoList.addAll(permissionDtos);
            groupPmDto.setId(groupPmEntity.getId());
            groupPmDto.setCode(groupPmEntity.getCode());
            groupPmDto.setName(groupPmEntity.getName());
            groupPmDto.setPermissionId(groupPmEntity.getPermissionEntities().stream()
                                        .map(permissionEntity -> permissionEntity.getId()).collect(Collectors.toList()));
            groupPmDto.setListPermission(permissionDtoList);
            groupPmDtoList.add(groupPmDto);
        }
        return groupPmDtoList;
    }

    @Override
    public GroupPmDto findById(Long id) {
        Optional<GroupPmEntity> entity = repository.findById(id);
        GroupPmDto dto= converter.toDto(entity);
        return dto;
    }

    @Override
    public GroupPmDto updatePermission(GroupPmDto groupPmDto) {
        GroupPmEntity groupPmEntity = repository.findById(groupPmDto.getId()).get();
        List<PermissionEntity> permissionEntities = permissionRepository.findAllById(groupPmDto.getPermissionId());
        groupPmEntity.setPermissionEntities(permissionEntities);
        repository.save(groupPmEntity);
        return groupPmDto;
    }
}
