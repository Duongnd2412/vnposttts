package com.vnpost.demo.sevice.impl;

import com.vnpost.demo.converter.GroupCategoryConverter;
import com.vnpost.demo.dto.CategoryDto;
import com.vnpost.demo.dto.GroupCategoryDto;
import com.vnpost.demo.dto.NewDto;
import com.vnpost.demo.entity.CategoryEntity;
import com.vnpost.demo.entity.GroupCategoryEntity;
import com.vnpost.demo.repository.GroupCategoryRepository;
import com.vnpost.demo.sevice.GroupCategoryService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class GroupCategoryImpl implements GroupCategoryService {
    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private GroupCategoryRepository groupRepository;

    @Override
    public int getTotalItem() {
        return (int) groupRepository.count();
    }

    @Override
    public List<GroupCategoryDto> findAllNew() {
        List<GroupCategoryDto> groupCategoryDtoList = new ArrayList<>();
        List<GroupCategoryEntity> entityList = groupRepository.findAll();
        for (GroupCategoryEntity groupCategoryEntity : entityList){
            List<NewDto> listDto = new ArrayList<>();
            List<String> nameCate = new ArrayList<>();
            List<Long> idCate = new ArrayList<>();
            GroupCategoryDto groupCategoryDto = new GroupCategoryDto();
            List<CategoryEntity> categoryEntities = groupCategoryEntity.getCategoryEntities();
            for (CategoryEntity categoryEntity: categoryEntities){
                List<NewDto> newDtoList = categoryEntity.getNewEntities().stream()
                        .map(item -> modelMapper.map(item,NewDto.class)).collect(Collectors.toList());
                listDto.addAll(newDtoList);
                nameCate.add(categoryEntity.getName());
                idCate.add(categoryEntity.getId());
            }
            Collections.sort(listDto, (o1, o2) ->o2.getId().compareTo(o1.getId()));
            groupCategoryDto.setId(groupCategoryEntity.getId());
            groupCategoryDto.setCategoryName(nameCate);
            groupCategoryDto.setIds(idCate);
            groupCategoryDto.setName(groupCategoryEntity.getName());
            groupCategoryDto.setListNew(listDto);
            groupCategoryDtoList.add(groupCategoryDto);
        }
        return groupCategoryDtoList;
    }

    @Override
    public List<GroupCategoryDto> findAll(Pageable pageable) {
        List<GroupCategoryEntity> groupCategoryEntities = groupRepository.findAll(pageable).getContent();
        return groupCategoryEntities.stream().map(item -> modelMapper.map(item,GroupCategoryDto.class)).collect(Collectors.toList());
    }

    @Override
    public List<GroupCategoryDto> findAll() {
        List<GroupCategoryEntity> groupCategoryEntities = groupRepository.findAll();
        return groupCategoryEntities.stream().map(item -> modelMapper.map(item,GroupCategoryDto.class)).collect(Collectors.toList());

    }

    @Override
    public GroupCategoryDto findAllNewById(Long id) {
        Optional<GroupCategoryEntity> groupCategory= groupRepository.findById(id);
        List<CategoryEntity> categoryEntities = groupCategory.get().getCategoryEntities();
        List<NewDto> listDto = new ArrayList<>();
        for (CategoryEntity categoryEntity: categoryEntities){
            List<NewDto> newDtoList = categoryEntity.getNewEntities().stream()
                    .map(item -> modelMapper.map(item,NewDto.class)).collect(Collectors.toList());
            listDto.addAll(newDtoList);
        }
        Collections.sort(listDto, (o1, o2) -> o2.getId().compareTo(o1.getId()));
        GroupCategoryDto categoryDto = new GroupCategoryDto();
        categoryDto.setListNew(listDto);
        return categoryDto;
    }

    @Override
    public GroupCategoryDto findById(Long id) {
        Optional<GroupCategoryEntity> categoryEntity = groupRepository.findById(id);
        return categoryEntity
                .map(item ->modelMapper.map(item, GroupCategoryDto.class))
                .orElse(null);
    }

    @Override
    public GroupCategoryDto create(GroupCategoryDto groupCategoryDto) {
        GroupCategoryEntity entity = modelMapper.map(groupCategoryDto,GroupCategoryEntity.class);
        groupRepository.save(entity);
        return groupCategoryDto;
    }

    @Override
    public void delete(long[] ids) {
        for (Long id : ids){
            groupRepository.deleteById(id);
        }
    }

    @Override
    public GroupCategoryDto update(GroupCategoryDto groupCategoryDto) {
        GroupCategoryEntity categoryEntity = groupRepository.findById(groupCategoryDto.getId()).get();
        categoryEntity.setName(groupCategoryDto.getName());
        groupRepository.save(categoryEntity);
        return groupCategoryDto;
    }
}
