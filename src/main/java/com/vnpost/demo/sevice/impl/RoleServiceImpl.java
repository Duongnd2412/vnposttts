package com.vnpost.demo.sevice.impl;

import com.vnpost.demo.converter.RoleConverter;

import com.vnpost.demo.dto.RoleDto;
import com.vnpost.demo.entity.GroupPmEntity;
import com.vnpost.demo.entity.PermissionEntity;
import com.vnpost.demo.entity.RoleEntity;

import com.vnpost.demo.repository.GroupPmRepository;
import com.vnpost.demo.repository.PermissionRepository;
import com.vnpost.demo.repository.RoleRepository;
import com.vnpost.demo.sevice.RoleService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class RoleServiceImpl implements RoleService {
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private GroupPmRepository groupPm;
    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private RoleConverter converter;

    @Override
    public List<RoleDto> findAll() {
        List<RoleEntity>role = roleRepository.findAll();
        return role.stream()
                .map(roles -> modelMapper.map(roles, RoleDto.class))
                .collect(Collectors.toList());
    }

    @Override
    public RoleDto updatePermissionGr(RoleDto roleDto) {
        RoleEntity roleEntity = roleRepository.findById(roleDto.getId()).get();
        List<GroupPmEntity> groupPmEntities = groupPm.findAllById(roleDto.getGroupId());
        roleEntity.setGroupPmEntities(groupPmEntities);
        roleRepository.save(roleEntity);
        return roleDto;
    }

    @Override
    public RoleDto findById(Long id) {
        Optional<RoleEntity> roleEntity = roleRepository.findById(id);
        RoleDto roleDto= converter.toDto(roleEntity);
        return roleDto;
    }



}
