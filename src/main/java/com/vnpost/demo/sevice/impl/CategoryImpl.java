package com.vnpost.demo.sevice.impl;

import com.vnpost.demo.dto.CategoryDto;
import com.vnpost.demo.dto.NewDto;
import com.vnpost.demo.entity.CategoryEntity;
import com.vnpost.demo.entity.NewEntity;
import com.vnpost.demo.repository.CategoryRepository;
import com.vnpost.demo.repository.NewRepository;
import com.vnpost.demo.sevice.CategoryService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CategoryImpl implements CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private ModelMapper mapper;


    @Override
    public CategoryDto create(CategoryDto categoryDto) {
        CategoryEntity categoryEntity = mapper.map(categoryDto,CategoryEntity.class);
        categoryRepository.save(categoryEntity);
        return categoryDto;
    }

    @Override
    public void delete(long[] ids) {
        for (Long id : ids){
            categoryRepository.deleteById(id); 
        }
    }

    @Override
    public int getTotalItem() {
        return (int) categoryRepository.count();
    }

    @Override
    public CategoryDto update(CategoryDto categoryDto) {
        CategoryEntity categoryEntity = categoryRepository.findById(categoryDto.getId()).get();
        categoryEntity.setName(categoryDto.getName());
        categoryRepository.save(categoryEntity);
        return categoryDto;
    }

    @Override
    public List<CategoryDto> findAll() {
        List<CategoryEntity> categoryEntity = categoryRepository.findAll();
        return categoryEntity.stream()
                .map(category -> mapper.map(category,CategoryDto.class))
                .collect(Collectors.toList());
    }

    @Override
    public List<CategoryDto> findAll(Pageable pageable) {
        List<CategoryEntity> categoryEntity = categoryRepository.findAll(pageable).getContent();
        return categoryEntity.stream()
                .map(category -> mapper.map(category,CategoryDto.class))
                .collect(Collectors.toList());
    }

    @Override
    public CategoryDto findById(Long id) {
        Optional<CategoryEntity> categoryEntity = categoryRepository.findById(id);
        return categoryEntity
                .map(item ->mapper.map(item, CategoryDto.class))
                .orElse(null);
    }

    @Override
    public CategoryDto findAllNewById(Long id) {
        Optional<CategoryEntity> categoryEntity = categoryRepository.findById(id);
        List<NewDto> newDtoList = categoryEntity.get().getNewEntities()
                                    .stream().map(item -> mapper.map(item,NewDto.class)).collect(Collectors.toList());
        CategoryDto categoryDto = new CategoryDto();
        categoryDto.setListNew(newDtoList);
        return categoryDto ;
    }
}
