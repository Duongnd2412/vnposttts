package com.vnpost.demo.sevice;

import com.vnpost.demo.dto.NewDto;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface NewService  {
    NewDto findById(Long id);
    NewDto update(NewDto newDto);
    NewDto create(NewDto newDto);
    void delete(long[] ids);
    List<NewDto> findAll(Pageable pageable);
    int getTotalItem();
    List<NewDto> findAll(NewDto newDto);
    void countViews(Long id);

    List<NewDto> findAllByCategory(Long categoryId);
}
