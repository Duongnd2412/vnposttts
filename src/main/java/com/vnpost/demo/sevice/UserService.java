package com.vnpost.demo.sevice;


import com.vnpost.demo.dto.UserDto;

import java.util.List;

public interface UserService {

    UserDto update(UserDto userDto);
    void delete(long[] id);
    UserDto create(UserDto userDto);
    List<UserDto>findAll();
    UserDto findById(Long id);

    void changePass(UserDto userDto);
}
