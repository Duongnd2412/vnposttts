package com.vnpost.demo.sevice;

import com.vnpost.demo.dto.CategoryDto;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface CategoryService {
    CategoryDto create(CategoryDto categoryDto);
    void delete(long[] ids);
    int getTotalItem();
    CategoryDto update(CategoryDto categoryDto);
    List<CategoryDto> findAll();
    List<CategoryDto> findAll(Pageable pageable);
    CategoryDto findById(Long id);
    CategoryDto findAllNewById(Long id);
}
