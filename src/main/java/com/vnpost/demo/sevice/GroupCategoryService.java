package com.vnpost.demo.sevice;

import com.vnpost.demo.dto.CategoryDto;
import com.vnpost.demo.dto.GroupCategoryDto;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.List;

public interface GroupCategoryService {
    int getTotalItem();
    List<GroupCategoryDto> findAllNew();
    List<GroupCategoryDto> findAll(Pageable pageable);
    List<GroupCategoryDto> findAll();
    GroupCategoryDto findAllNewById(Long id);
    GroupCategoryDto findById(Long id);
    GroupCategoryDto create(GroupCategoryDto groupCategoryDto);
    void delete(long[] ids);
    GroupCategoryDto update(GroupCategoryDto groupCategoryDto);

}
