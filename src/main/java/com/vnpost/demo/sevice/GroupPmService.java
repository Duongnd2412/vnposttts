package com.vnpost.demo.sevice;

import com.vnpost.demo.dto.GroupPmDto;

import java.util.List;

public interface GroupPmService {

    List<GroupPmDto> findAll();
    GroupPmDto findById(Long id);
    GroupPmDto updatePermission(GroupPmDto groupPmDto);
}
