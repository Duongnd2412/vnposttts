<%@ include file="/common/taglib.jsp"%>
<%@ page pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>VnPost | Tin tức</title>

    <!-- Bootstrap core CSS -->
    <!-- <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet"> -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">

    <!-- Custom styles for this template -->
    <link href="<c:url value='/css/web/tin-tuc.css'/>" rel="stylesheet">
    <!-- icon -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


</head>

<body>

<!-- Navigation -->
<%@ include file="/common/web/header1.jsp"%>

<!-- Page Content -->
<div class="container">
    <div class="row" style="width: 1300px;">
        <div class="col-sm-3">
            <h1 class="my-4"></h1>
            <div class="list-group nav-link" style="background-color: #fffad5;">
                <a style="color: #fcb71e;"><b>CHUYÊN MỤC</b></a>
                <ul>
                    <c:forEach items="${group}" var="item">
                        <a href="/bai-viet/nhom/the-loai/${item.id}">${item.name}</a>
                        <hr>
                        <ul>
                            <c:forEach items="${item.categoryName}" var="item1" varStatus="loop">
                                <li>
                                    <a href="/bai-viet/the-loai/${item.ids[loop.index]}">
                                        <div class="clearfix">
                                                ${item1}
                                        </div>
                                    </a>
                                    <hr>
                                </li>
                            </c:forEach>
                        </ul>
                    </c:forEach>
                </ul>
                <div style="background-color: #ff9900; color: white;">
                    <a>Đường dây nóng hỗ trợ <br><h1 style="font-size: 35px;">1900 54 54 81</h1></a>
                </div>

            </div>

        </div>
        <!-- /.col-lg-3 -->

        <div class="col-sm-9" style="padding-top: 20px">
            <div style="width: 900px;">
                <c:forEach var="item" items="${groupId.listNew}">
                    <div class=" news-item row">
                        <div class="col-sm-4">
                            <img src="${item.thumbnail}" alt="" style="width: 200px;height: 150px">
                        </div>
                        <div class="col">
                                <a href="/bai-viet/chi-tiet/${item.id}">
                                    <h6>${item.title}</h6>
                                </a>
                            <label class="limit-text">
                                    ${item.shortDescription}
                            </label>
                            <span ><fmt:formatDate type = "both"
                                                    dateStyle = "short" timeStyle = "short" pattern="dd-M-yyyy" value = "${item.createdDate}" />
                            </span>
                            <hr>
                        </div>
                    </div>
                    <br>
                </c:forEach>
            </div>

            <!-- /.row -->

        </div>
        <!-- /.col-lg-9 -->

    </div>
    <!-- /.row -->
</div>
<!-- /.container -->

<!-- Footer -->

<%@ include file="/common/web/footer.jsp"%>



<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>


</body>

</html>
