<%@ include file="/common/taglib.jsp"%>
<%@ page pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>VnPost | ${news.title}</title>

    <!-- Bootstrap core CSS -->
    <!-- <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet"> -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">

    <!-- Custom styles for this template -->
    <link href="<c:url value='/css/web/tin-tuc.css'/>" rel="stylesheet">
    <!-- icon -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


</head>

<body>

<!-- Navigation -->
<%@ include file="/common/web/header1.jsp"%>


<!-- Page Content -->
<div class="container" >
    <div class="row" style="width: 1300px;">
        <div class="col-sm-3">
            <h1 class="my-4"></h1>
            <div class="list-group nav-link" style="background-color: #fffad5;">
                <a style="color: #fcb71e;"><b>CHUYÊN MỤC</b></a>
                <ul>
                    <c:forEach items="${group}" var="item">
                        <a href="#">${item.name}</a>
                        <hr>
                        <ul>
                            <c:forEach items="${item.categoryName}" var="item1" varStatus="loop">
                                <li>
                                    <a href="/bai-viet/the-loai/${item.ids[loop.index]}">
                                        <div class="clearfix">
                                                ${item1}
                                        </div>
                                    </a>
                                    <hr>
                                </li>
                            </c:forEach>
                        </ul>
                    </c:forEach>
                </ul>
                <div style="background-color: #ff9900; color: white;">
                    <a>Đường dây nóng hỗ trợ <br><h1 style="font-size: 35px;">1900 54 54 81</h1></a>
                </div>

            </div>

        </div>
        <!-- /.col-lg-3 -->

        <div class="col-sm-9">
            <div style="width: 900px;">
                <h2>${news.title}</h2>
                <label class="news-date"><fmt:formatDate type = "both"
                                 dateStyle = "short" timeStyle = "short" pattern="dd-M-yyyy" value = "${news.createdDate}" />
                </label>
                <div class="col-sm-12 col-md-12" id="content">
                    ${news.content}
                </div>
                <div >
                    <label class="view">
                        <i class="fa fa-eye"></i>Lượt xem: ${news.count}
                    </label>
                </div>
            </div>
            <div class="col-sm-12 news-footer">
                <div class="header-news-footer">
<%--                    <h2>Các tin khác</h2>--%>
                </div>
                <div class="body-news-footer">
                    <ul>
<%--                        <c:forEach items="${itemNew}" var="item">--%>
<%--                            <li><a href="/bai-viet/chi-tiet/${item.id}">${item.title}</a>--%>
<%--                                <label class="news-date" style="margin-left: 20px;"><fmt:formatDate type = "both"--%>
<%--                                    dateStyle = "short" timeStyle = "short" pattern="dd-M-yyyy" value = "${item.createdDate}" />--%>
<%--                                </label></li>--%>
<%--                        </c:forEach>--%>

                    </ul>
                </div>
            </div>
            <!-- /.row -->

        </div>
        <!-- /.col-lg-9 -->

    </div>
    <!-- /.row -->
</div>
<!-- /.container -->

<!-- Footer -->
<%@ include file="/common/web/footer.jsp"%>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>


</body>

</html>
