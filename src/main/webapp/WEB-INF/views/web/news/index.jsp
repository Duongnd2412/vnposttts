<%@ include file="/common/taglib.jsp"%>
<%@ page pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>VnPost | Tin tức</title>

    <!-- Bootstrap core CSS -->
    <!-- <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet"> -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">

    <!-- Custom styles for this template -->
    <link href="<c:url value='/css/web/tin-tuc.css'/>" rel="stylesheet">
    <!-- icon -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


</head>

<body>

<!-- Navigation -->
<%@ include file="/common/web/header1.jsp"%>

<!-- Page Content -->
<div class="container mt-4">
    <div class="row">
        <div class="col-md-3 col-sm-12">

            <div class="list-group nav-link" style="background-color: #fffad5;">
                <a style="color: #fcb71e;"><b>CHUYÊN MỤC</b>
                    <hr></a>
                <ul>
                    <c:forEach items="${group}" var="item">
                        <a href="/bai-viet/nhom/the-loai/${item.id}">${item.name}</a>
                        <hr>
                        <ul>
                            <c:forEach items="${item.categoryName}" var="item1" varStatus="loop" >
                                <li>
                                    <a href="/bai-viet/the-loai/${item.ids[loop.index]}">
                                        <div class="clearfix">
                                                ${item1}
                                        </div>
                                    </a>
                                    <hr>
                                </li>
                            </c:forEach>
                        </ul>
                    </c:forEach>
                </ul>
                <div style="background-color: #ff9900; color: white;">
                    <a>Đường dây nóng hỗ trợ <br><h1 style="font-size: 35px;">1900 54 54 81</h1></a>
                </div>
            </div>
        </div>
        <!-- /.col-lg-3 -->

        <div class="col-md-9 col-sm-12">
            <c:forEach items="${group}" var="item">
                <div class="head-top mt-3">
                    <a style="float: left" href="/bai-viet/nhom/the-loai/${item.id}"><b>${item.name}</b></a><br>
                    <c:forEach items="${item.categoryName}" var="itemCategory" varStatus="loop">
                        <a href="/bai-viet/the-loai/${item.ids[loop.index]}">${itemCategory}
                        </a>
                    </c:forEach>
                </div>
                <br>
                <div class="row mt-2" >
                    <div class="col-md-5 col-sm-12" >
                        <c:forEach items="${item.listNew}" var="itemNew" begin="0" end="0">
                            <div class="card h-100 boder-none">
                                <a href="/bai-viet/chi-tiet/${itemNew.id}">${itemNew.title}</a>
                                <img class="card-img-top" src="${itemNew.thumbnail}" style="width: 350px;height: 195px">
                                <span style="font-size: 13px">${itemNew.shortDescription}</span>
                            </div>
                        </c:forEach>
                    </div>
                    <div class="col-md-4 col-sm-12">
                        <div class="card h-100 border-respon">
                            <ul style="padding: 0 10px">
                                <c:forEach items="${item.listNew}" var="itemNew" begin="2" end="5">
                                    <li>
                                        <a style="font-size: 13px" href="/bai-viet/chi-tiet/${itemNew.id}">${itemNew.title}</a>
                                        <hr>
                                    </li>
                                </c:forEach>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-12">
                        <div class="card h-100 boder-none">
                            <c:forEach items="${item.listNew}" var="itemNew" begin="1" end="1">
                                <img class="card-img-top"style="height: 125px;width: 190px" src="${itemNew.thumbnail}">
                                <a href="/bai-viet/chi-tiet/${itemNew.id}"><h6>${itemNew.title}</h6></a>
                            </c:forEach>
                        </div>
                    </div>
                </div>
                </c:forEach>
            <!-- /.row -->

        </div>
        <!-- /.col-lg-9 -->
    </div>
    <!-- /.row -->
</div>
<!-- /.container -->

<!-- Footer -->
<%@ include file="/common/web/footer.jsp"%>

<!-- Bootstrap core JavaScript -->
<!-- <script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script> -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>


</body>

</html>
