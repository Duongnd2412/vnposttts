<%@ include file="/common/taglib.jsp"%>
<%@ page pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Bưu điện Việt Nam - VnPost</title>


    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">

    <!-- Custom styles for this template -->
    <link href="<c:url value='/css/web/trang-chu.css'/>" rel="stylesheet">
    <!-- icon -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


</head>

<body>
<%--header--%>
<%@ include file="/common/web/header1.jsp"%>
<!-- Page Content -->
<div id="carouselExampleIndicators" class="carousel slide my-4" data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner" role="listbox">
        <div class="carousel-item active">
            <img class="d-block img-fluid" src="<c:url value='/image/vnpost1.PNG'/>" alt="First slide">
        </div>
        <div class="carousel-item">
            <img class="d-block img-fluid" src="<c:url value='/image/vnpost2.PNG'/>" alt="Second slide">
        </div>
        <div class="carousel-item">
            <img class="d-block img-fluid" src="<c:url value='/image/vnpost3.PNG'/>" alt="Third slide">
        </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>

<div class="container-fluid">
    <div class="row ">
        <!--  /.col-lg-3 -->
        <div style="padding: 0 10%">
            <div class="row">
                <div class="col-md-4 col-sm-12 mb-4 ">
                    <b style="font-size: 20px;">CHUYÊN TRANG</b>
                    <div class="card h-100" style="background-color: #f7f7f7;">
                        <ul style="list-style: none; width: 100%;">
                            <li>
                                <a href="#" class="col-body">
                                    <div class="clearfix row col-left">
                                        <div class="col-left col-sm-2">
                                            <img src="http://www.vnpost.vn/Portals/0/banner%20logo/Recycle.png">
                                        </div>
                                        <div class="col-sm-10 col-left" style="color: black;">
                                            <label style="font-size: 17px;"><b> Nói không với rác thải nhựa</b></label><br>
                                            <span style="font-size: 13px;">Bưu điện Việt Nam vì một cuộc sống xanh</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="col-body">
                                    <div class="clearfix row col-left">
                                        <div class="col-left col-sm-2">
                                            <img src="http://www.vnpost.vn/Portals/0/newspaper.png">
                                        </div>
                                        <div class="col-sm-10 col-left" style="color: black;">
                                            <label style="font-size: 17px;"><b>Tổng hợp báo chí</b></label><br>
                                            <span style="font-size: 13px;">Tổng hợp báo chí ngành bưu điện</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="col-body">
                                    <div class="clearfix row col-left">
                                        <div class="col-left col-sm-2">
                                            <img src="http://www.vnpost.vn/Portals/0/quan-ly-chat-luong.png">
                                        </div>
                                        <div class="col-sm-10 col-left" style="color: black;">
                                            <label style="font-size: 17px;"><b>Quản lý chất lượng</b></label><br>
                                            <span style="font-size: 13px;">Văn bản quản lý chất lượng dịch vụ</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="col-body">
                                    <div class="clearfix row col-left">
                                        <div class="col-left col-sm-2">
                                            <img src="http://www.vnpost.vn/Portals/0/stamp.png">
                                        </div>
                                        <div class="col-sm-10 col-left" style="color: black;">
                                            <label style="font-size: 17px;"><b> Tem bưu chính</b></label><br>
                                            <span style="font-size: 13px;">Văn bản quản lý tem bưu chính</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="col-body">
                                    <div class="clearfix row col-left">
                                        <div class="col-left col-sm-2">
                                            <img src="http://www.vnpost.vn/Portals/0/thi-dua-khen-thuong.png">
                                        </div>
                                        <div class="col-sm-10 col-left" style="color: black;">
                                            <label style="font-size: 17px;"><b>Thi đua-Khen thưởng</b></label><br>
                                            <span style="font-size: 13px;">Tổng hợp thông tin thi đua khen thưởng</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="col-body">
                                    <div class="clearfix row col-left">
                                        <div class="col-left col-sm-2">
                                        </div>
                                        <div class="col-sm-10 col-left" style="color: black;">
                                            <label style="font-size: 17px;"><b>Đào tạo</b></label><br>
                                            <span style="font-size: 13px;">Chương trình đào tạo cho nhân viên</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="col-body">
                                    <div class="clearfix row col-left">
                                        <div class="col-left col-sm-2">
                                            <img src="http://www.vnpost.vn/Portals/0/folder.png">
                                        </div>
                                        <div class="col-sm-10 col-left" style="color: black;">
                                            <label style="font-size: 17px;"><b>Văn bản pháp lý</b></label><br>
                                            <span style="font-size: 13px;">Văn bản pháp lý và các thông tin liên quan</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="col-body">
                                    <div class="clearfix row col-left">
                                        <div class="col-left col-sm-2">
                                            <img src="http://www.vnpost.vn/Portals/0/Images/Icon-Dang.png" style="height: 35px;">
                                        </div>
                                        <div class="col-sm-10 col-left" style="color: black;">
                                            <label style="font-size: 17px;"><b>Đại hội đảng</b></label><br>
                                            <span style="font-size: 13px;">Hướng tới Đại hội Đại biểu Đảng bộ TCT lần II</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="col-body">
                                    <div class="clearfix row col-left">
                                        <div class="col-left col-sm-2">

                                        </div>
                                        <div class="col-sm-10 col-left" style="color: black;">
                                            <label style="font-size: 17px;"><b> Thông tin doanh nghiệp</b></label><br>
                                            <span style="font-size: 13px;">Chuyên trang thông tin doanh nghiệp</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-8 col-sm-12 row mb-4 ">
                    <div class="col-md-6 col-sm-12  mb-4">
                        <b style="font-size: 20px;">TIN TỨC</b>
                        <div class="card h-100" style="border: none;">
                            <img class="card-img-top" src="http://www.vnpost.vn/ImageCaching.ashx?file=%2fPortals%2f0%2fanh+tin+tuc%2f2020-1%2fThang+6%2fphat-huy-vai-tro-cua-buu-dien-trong-chinh-quyen-dien-tu-2.jpg&size=3&ver=4" alt="">
                            <div>
                                <a href="#" style="color: black"><b>Kỳ Cuối: Phát huy vai trò của bưu điện trong chính quyền điện tử</b></a><br>
                                <span style="font-size: 13px">19/06/2020</span>
                                <p class="card-text" style="font-size: 13px">Mạng lưới bưu điện đã thể hiện rõ vai trò cánh tay nối dài của các cơ quan hành chính nhà nước. Mục tiêu xa hơn nữa, bưu điện sẽ là nhân tố tích cực và quan trọng của chính quyền điện tử, chính phủ điện tử</p>
                            </div>

                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12  mb-4">
                        <br>
                        <div  class="row">
                            <div class="col-md-4 col-sm-12">
                                <img class="img1" src="http://www.vnpost.vn/ImageCaching.ashx?file=%2fPortals%2f0%2fanh+tin+tuc%2f2020-1%2fThang+6%2fvov2.jpg&size=3&ver=2">
                            </div>
                            <div class="col-md-8 col-sm-12 tex1" >
                                <a href="#"><b>Bưu điện Việt Nam thăm, chúc mừng các cơ quan báo chí nhân dịp kỷ niệm 95 năm ngày Báo chí cách mạng Việt Nam</b></a>
                            </div>
                        </div>
                        <div  class="row">
                            <div class="col-md-4 col-sm-12">
                                <img class="img1" src="http://www.vnpost.vn/ImageCaching.ashx?file=%2fPortals%2f0%2fanh+tin+tuc%2f2020-1%2fThang+6%2fphat-huy-vai-tro-cua-buu-dien-trong-chinh-quyen-dien-tu-2.jpg&size=3&ver=4">
                            </div>
                            <div class="col-md-8 col-sm-12 tex1" >
                                <a href="#"><b>Kỳ Cuối: Phát huy vai trò của bưu điện trong chính quyền điện tử</b></a>
                            </div>
                        </div>
                        <div  class="row">
                            <div class="col-md-4 col-sm-12">
                                <img class="img1" src="http://www.vnpost.vn/ImageCaching.ashx?file=%2fPortals%2f0%2fanh+tin+tuc%2f2020-1%2fThang+6%2fHoi+nghi+Cum+CD+so+5.jpg&size=3&ver=2">
                            </div>
                            <div class="col-md-8 col-sm-12 tex1" >
                                <a href="#"><b>Cụm Công đoàn số 5: Thúc đẩy kinh doanh các dịch vụ quyết tâm hoàn thành kế hoạch năm 2020</b></a>
                            </div>
                        </div>
                        <div  class="row">
                            <div class="col-md-4 col-sm-12">
                                <img class="img1" src="http://www.vnpost.vn/ImageCaching.ashx?file=%2fPortals%2f0%2fanh+tin+tuc%2f2020-1%2fThang+6%2fky-ii-canh-tay-noi-dai-cua-cac-so-nganh-1.jpg&size=3&ver=2">
                            </div>
                            <div class="col-md-8 col-sm-12 tex1" >
                                <a href="#"><b>Kỳ II: Dịch vụ bưu chính - hành chính công: Cánh tay nối dài của các sở, ngành</b></a>
                            </div>
                        </div>
                        <div  class="row">
                            <div class="col-md-4 col-sm-12">
                                <img class="img1" src="http://www.vnpost.vn/ImageCaching.ashx?file=%2fPortals%2f0%2fanh+tin+tuc%2f2020-1%2fThang+6%2fdich-vu-buu-chinh-hanh-chinh-cong-diem-cong-cua-buu-dien.jpg&size=3&ver=3">
                            </div>
                            <div class="col-md-8 col-sm-12 tex1" >
                                <a href="#"><b>Dịch vụ bưu chính - hành chính công: “Điểm cộng” của bưu điện</b></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.col-lg-9 -->
    </div>
    <!-- /.row -->
    <br>
    <div style="background-color: #f6f6f6" class="row">
        <div class="col-md-2"></div>
        <div class="col-md-3 col-sm-12">
            <a href="#"><img class="imgft" src="http://www.vnpost.vn/ImageCaching.ashx?file=%2fPortals%2f0%2fDichVu%2f02+-+Buu+dien+Viet+Nam+-+Product.png&size=3&ver=5">
                <br><b class="fontB" style="font-size: 20px;">BƯU CHÍNH CHUYỂN PHÁT</b>
            </a><br>
            <br>
            <br>
            <ul>
                <li><a class="fontB" href="#">Bưu chính chuyển phát trong nước</a></li>
                <li><a class="fontB" href="#">Bưu chính chuyển phát Quốc tế</a></li>
            </ul>
        </div>
        <div class="col-md-3 col-sm-12">
            <a href="#"><img class="imgft" src="http://www.vnpost.vn/ImageCaching.ashx?file=%2fPortals%2f0%2fDichVu%2fA022_C056_0106MU.0000339-edit.png&size=3&ver=5">
                <br><b class="fontB" style="font-size: 20px;">TÀI CHÍNH BƯU CHÍNH</b>
            </a><br>
            <br>
            <br>
            <ul>
                <li><a class="fontB" href="#">Bảo hiểm phi nhân thọ PTI</a></li>
                <li><a class="fontB" href="#">Thu hộ-Chi hộ</a></li>
                <li><a class="fontB" href="#">Đại lý bảo hiểm nhân thọ(Dai-ichi)</a></li>
            </ul>
        </div>
        <div class="col-md-3 col-sm-12">
            <a href="#"><img class="imgft" src="http://www.vnpost.vn/ImageCaching.ashx?file=%2fPortals%2f0%2fDichVu%2f02+-+Buu+dien+Viet+Nam+(5)-+Product.png&size=3&ver=6">
                <b class="fontB" style="font-size: 20px;"><br>PHÂN PHỐI-TRUYỀN THÔNG</b>
            </a><br>
            <br>
            <br>
            <ul>
                <li><a class="fontB" href="#">Sàn thương mại điện tử POSTMART</a></li>
                <li><a class="fontB" href="#">Truyền thông,quảng cáo</a></li>
                <li><a class="fontB" href="#">Phân phối xuất bản ấn phẩm</a></li>
            </ul>
        </div>
        <div class="col-md-1"></div>
    </div>

</div>
<!-- /.container -->
<div >
    <div>
        <div class="container">
            <div class="row" style="padding-top: 60px">
                <div class="col-md-6 col-sm-12">
                    <h4 style="color: #fdbe25">THƯ VIỆN VIDEO</h4>
                    <iframe width="100%" height="298" src="https://www.youtube.com/embed/iPEvFyikq-g" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    <h5 style="color: #fdbe25">Ra mắt nền tảng mã địa chỉ bưu chính - Vpostcode</h5>
                </div>
                <div class="col-md-6 col-sm-12">
                    <h4 style="color: #fdbe25">MUA SẮM TRỰC TUYẾN</h4>
                    <p style="font-size: 15px">Hiện tại chúng tôi có những gian hàng mua sắm online với đầy đủ những sản phẩm tiện ích, đa dạng. Hy vọng sẽ đem đến cho quý khách hàng những trải nghiệm mua sắm mới mẻ nhất. Hãy đến với hệ thống mua sắm trực tuyến của chúng tôi để tìm cho mình những sản phẩm thiết thực nhất</p>
                    <div class="row">
                        <div class="col-sm-4 text-ft">
                            <a href="#">
                                <img class="img-ft" src="http://www.vnpost.vn/ImageCaching.ashx?file=%2fPortals%2f0%2fImages%2fPostmart+thumb.jpg&size=2&ver=28" alt="">
                                <p class="text-center">SÀN THƯƠNG MẠI ĐIỆN TỬ POSTMART</p>
                            </a>
                        </div>
                        <div class="col-sm-4 text-ft">
                            <a href="#">
                                <img class="img-ft"src="http://www.vnpost.vn/ImageCaching.ashx?file=%2fPortals%2f0%2fLich+2019+(4).png&size=2&ver=31" alt="">
                                <p class="text-center">LỊCH TẾT</p>
                            </a>
                        </div>
                        <div class="col-sm-4 text-ft">
                            <a href="#">
                                <img class="img-ft"src="http://www.vnpost.vn/ImageCaching.ashx?file=%2fPortals%2f0%2fDataPost+(2).jpg&size=2&ver=31" alt="">
                                <p class="text-center ">DỊCH VỤ DATA POST</p>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Footer -->
<%@ include file="/common/web/footer.jsp"%>
<!-- Bootstrap core JavaScript -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>

</body>

</html>
