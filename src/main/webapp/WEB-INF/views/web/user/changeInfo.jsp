<%@include file="/common/taglib.jsp" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Chuyên mục</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <%--    <script language="javascript" src="http://code.jquery.com/jquery-2.0.0.min.js"></script>--%>
    <link href="<c:url value='/common/admin/fontawesome-free/css/all.min.css'/>" rel="stylesheet" type="text/css">

    <!-- Custom styles for this template-->
    <link href="<c:url value='/css/admin/sb-admin.css'/>" rel="stylesheet">
</head>
<body>
<%@include file="/common/admin/header.jsp"%>

<div id="wrapper">
    <%@include file="/common/admin/menu.jsp"%>
    <div class="main-content">
        <div class="main-content-inner">
            <div class="breadcrumbs" id="breadcrumbs" >
                <script type="text/javascript">
                    try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
                </script>
                <ul class="breadcrumb">
                    <li>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="<c:url value="/admin"/>"><i class="ace-icon fa fa-home home-icon"></i>Trang chủ</a>
                            </li>
                            <li class="breadcrumb-item active">Đổi mật khẩu</li>
                        </ol>
                    </li>
                </ul><!-- /.breadcrumb -->
            </div>

            <div class="page-content" style="width: 900px">
                <div class="page-header">
                    <h1>
                        <small>
                            <a href="#"><i class="ace-icon fa fa-angle-double-right"></i>
                                Tài khoản</a>
                        </small>
                        <small>
                            <a href="#"><i class="ace-icon fa fa-angle-double-right"></i>
                                EDIT</a>
                        </small>
                    </h1>
                </div><!-- /.page-header -->
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <form:form method="POST" action="/admin/user/changePassword" modelAttribute="viewmodel" class="form-horizontal" role="form" id="formEdit">
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="oldPassword">Nhập lại mật khẩu hiện tại</label>
                            <div class="col-sm-9">
                                <form:input type="password" path="oldPassword" required="required"   class="col-xs-10 col-sm-5" ></form:input>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="password">Nhập mật khẩu mới</label>
                            <div class="col-sm-9">
                                <form:input type="password" path="password"  required="required" class="col-xs-10 col-sm-5" ></form:input>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="confirmPassword">Nhập lại mật khẩu</label>
                            <div class="col-sm-9">
                                <form:input type="password" path="confirmPassword" onchange=""  required="required" class="col-xs-10 col-sm-5" ></form:input>
                            </div>
                            <span id="error" style="display:none">Mật khẩu không khớp</span>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-5">
                                <button type="submit" id="btnAdd"  class="btn btn-sm btn-success " style="left:450px;">
                                    Submit
                                    <i class="ace-icon fa fa-arrow-right icon-on-right bigger-110"></i>
                                </button>
                            </div>
                        </div>
                    </form:form>
                </div><!-- /.col -->
                </div><!-- /.row -->
        </div>
    </div>
        <script>
            function confirmPass() {
                var pass = $("#password").val();
                if(pass==$('#confirmPassword')) {
                    $("#error").show();
                    return;
                }else {
                    document.getElementById('btnAdd').disabled = false;
                }
            };
        </script>
</body>
</html>
