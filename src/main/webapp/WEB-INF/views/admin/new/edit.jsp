<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/common/taglib.jsp"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:url var="urlList" value="/admin/new/list?page=1&limit=5"/>
<c:url var="urlEdit" value="/admin/api/new/edit"/>
<html>
<head>
    <title>Chỉnh sửa bài viết</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<%--    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>--%>
<%--    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>--%>
<%--    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>--%>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>

<%--    <script language="javascript" src="http://code.jquery.com/jquery-2.0.0.min.js"></script>--%>
    <link href="<c:url value='/common/admin/fontawesome-free/css/all.min.css'/>" rel="stylesheet" type="text/css">

    <!-- Custom styles for this template-->
    <link href="<c:url value='/css/admin/sb-admin.css'/>" rel="stylesheet">
    <link href="<c:url value='/css/admin/user.css'/>" rel="stylesheet">

</head>
<body>
<%@include file="/common/admin/header.jsp"%>

<div id="wrapper">
    <%@include file="/common/admin/menu.jsp"%>
    <div class="main-content" style="width: 100%">
        <div class="main-content-inner">
            <div class="breadcrumbs" id="breadcrumbs">
                <script type="text/javascript">
                    try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
                </script>
                <ul class="breadcrumb">
                    <li>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="<c:url value="/admin"/>"><i class="ace-icon fa fa-home home-icon"></i><b>Trang chủ</b></a>
                            </li>
                            <li class="breadcrumb-item active"><b>Chỉnh sửa bài viết</b></li>
                        </ol>
                    </li>
                </ul><!-- /.breadcrumb -->
            </div>
            <div class="page-content">
                <div class="row">
                    <div class="col-md-12">
                        <c:if test="${not empty message}">
                            <div class="alert alert-${alert}">
                                    ${message}
                            </div>
                        </c:if>
                            <form:form id="formSubmit" role="form" modelAttribute="editNew" cssClass="form-horizontal container" >
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right">Thể loại</label>
                                    <div class="col-sm-12">
                                        <select class="form-control" id="categoryName" name="categoryName">
                                                <c:forEach var="item" items="${category}">
                                                    <c:if test="${item.id == editNew.categoryId}">
                                                        <option value="${item.name} " selected="selected">${item.name}</option>
                                                    </c:if>
                                                    <c:if test="${item.id != editNew.categoryId}">
                                                        <option value="${item.name} ">${item.name}</option>
                                                    </c:if>
                                                </c:forEach>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right">Tiêu đề</label>
                                    <div class="col-sm-12">
                                        <form:textarea path="title" cssClass="form-control" id="title" cssStyle="height: 80px"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right">Hình đại diện</label>
                                    <div class="col-sm-12">
                                        <div class="avatar">
                                            <img id="thumbnail"
                                                 src="${editNew.thumbnail}"
                                                 class="img-fluid" style="max-width: 300px; max-height: 300px;" />
                                        </div>
                                        <div class="file-field">
                                            <p>
                                                <strong id="Ithumbnail">Chọn ảnh</strong><br />
                                                <button
                                                    class="btn btn-primary btn-sm waves-effect waves-light"
                                                    type="button" value="Browse Image"
                                                    onclick="BrowseServer( 'Images:/', 'Ithumbnail' );" >Browse Image</button>
                                            </p>
                                        </div>
                                        <form:hidden  path="thumbnail" id="image_src"/>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right">Mô tả ngắn</label>
                                    <div class="col-sm-12" >
                                        <form:textarea path="shortDescription" cssClass="form-control" id="shortDescription" cssStyle="height: 100px" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right">Nội dung</label>
                                    <div class="col-sm-12">
                                        <form:textarea path="content"  id="content" />
                                    </div>
                                </div>
                                <form:hidden path="id" id="newId"/>
                                <div class="form-group">
                                    <div class="col-sm-5">
                                        <button type="submit"  class="btn btn-white btn-warning btn-bold" value="Cập nhật bài viết" id="btnUpdateNew"/>Cập nhật
                                        <i class="ace-icon fa fa-arrow-right icon-on-right bigger-110"></i></button>
                                    </div>
                                </div>
                            </form:form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<c:url value='/ckeditor/ckeditor.js'/>"></script>
<script src="<c:url value='/ckfinder/ckfinder.js'/>"></script>
<script >
	var editor = '';
    $(document).ready(function(){
        editor= CKEDITOR.replace('content',
            {
                filebrowserBrowseUrl : '/ckfinder/ckfinder.html',
                filebrowserImageBrowseUrl : '/ckfinder/ckfinder.html?type=Images',
                filebrowserFlashBrowseUrl : '/ckfinder/ckfinder.html?type=Flash',
                filebrowserUploadUrl : '/ckfinder/core/connector/java/connector.java?command=QuickUpload&type=Files',
                filebrowserImageUploadUrl : '/ckfinder/core/connector/java/connector.java?command=QuickUpload&type=Images',
                filebrowserFlashUploadUrl : '/ckfinder/core/connector/java/connector.java?command=QuickUpload&type=Flash'
            });
    });
    /*Avatar start*/
    function BrowseServer(startupPath, functionData) {
        // You can use the "CKFinder" class to render CKFinder in a page:
        var finder = new CKFinder();

        // The path for the installation of CKFinder (default = "/ckfinder/").
        finder.basePath = '../';

        //Startup path in a form: "Type:/path/to/directory/"
        finder.startupPath = startupPath;

        // Name of a function which is called when a file is selected in CKFinder.
        finder.selectActionFunction = SetFileField;

        // Additional data to be passed to the selectActionFunction in a second argument.
        // We'll use this feature to pass the Id of a field that will be updated.
        finder.selectActionData = functionData;

        // Name of a function which is called when a thumbnail is selected in CKFinder. Preview img
        // finder.selectThumbnailActionFunction = ShowThumbnails;

        // Launch CKFinder
        finder.popup();
    }

    function SetFileField(fileUrl, data) {
        document.getElementById(data["selectActionData"]).innerHTML = this
            .getSelectedFile().name;
        document.getElementById("thumbnail").src = fileUrl;
        $('#thumbnail').val(fileUrl);
        $('#image_src').val(fileUrl);
    }

    $('#btnUpdateNew').click(function (e) {
        e.preventDefault();
        var data = {};
        var formData = $('#formSubmit').serializeArray();
        $.each(formData, function (i, v) {
            data[""+v.name+""] = v.value;
        });
        data["content"] = editor.getData();
        var id = $('#id').val();
        updateNew(data);
    });
    function updateNew(data) {
        $.ajax({
            url: '${urlEdit}',
            type: 'PUT',
            contentType: 'application/json',
            data: JSON.stringify(data),
            dataType: 'json',
            success: function () {
                window.location.href = "${urlList}&message=update_success";
            },
            error: function () {
                window.location.href = "${urlList}&message=error_system";
            }
        });
    }
</script>
</body>
</html>
