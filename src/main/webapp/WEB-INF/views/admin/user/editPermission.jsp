<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/common/taglib.jsp"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:url var="urlList" value="/admin/user/role"/>
<c:url var="APIurl" value="/admin/api/user/permission/${groups.id}"/>
<html>
<head>
    <title>Chỉnh sửa quyền chi tiết</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <%--    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>--%>
    <%--    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>--%>
    <%--    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>--%>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
    <link href="<c:url value='/common/admin/fontawesome-free/css/all.min.css'/>" rel="stylesheet" type="text/css">
    <!-- Custom styles for this template-->
    <link href="<c:url value='/css/admin/sb-admin.css'/>" rel="stylesheet">
</head>
<body>
<%@include file="/common/admin/header.jsp"%>

<div id="wrapper">
    <%@include file="/common/admin/menu.jsp"%>
    <div class="main-content container" style="width: 100%">
        <div class="main-content-inner">
            <div class="breadcrumbs" id="breadcrumbs">
                <script type="text/javascript">
                    try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
                </script>
                <ul class="breadcrumb">
                    <li>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="<c:url value="/admin"/>"><i class="ace-icon fa fa-home home-icon"></i><b>Trang chủ</b></a>
                            </li>
                            <li class="breadcrumb-item active"><b>Chỉnh sửa chi tiết quyền </b></li>
                        </ol>
                    </li>
                </ul><!-- /.breadcrumb -->
            </div>
            <div class="page-content">
                <div class="row">
                    <div class="col-md-12">
                        <c:if test="${not empty message}">
                            <div class="alert alert-${alert}">
                                    ${message}
                            </div>
                        </c:if>
                        <form class="form-horizontal" id="formSubmit" action="/admin/api/user/permission/${groups.id}"  style="padding-left: 30px">
                            <h1>${groups.name}</h1>
                            <br>
                            <c:forEach var="item" items="${permissions}">
                                <c:if test="${item.checked == 1}">
                                    <div class="form-check " >
                                        <input class="form-check-input" checked="checked" name="permissionId" type="checkbox" value="${item.id}" id="permissionId${item.id}">
                                        <label class="form-check-label" >
                                                ${item.name}
                                        </label>
                                    </div>
                                    <br>
                                </c:if>
                                <c:if test="${item.checked != 1}">
                                    <div class="form-check" >
                                        <input class="form-check-input"  name="permissionId" type="checkbox" value="${item.id}" id="permissionId${item.id}">
                                        <label class="form-check-label" >
                                                ${item.name}
                                        </label>
                                    </div>
                                    <br>
                                </c:if>
                            </c:forEach>
                            <hidden name="id" id="roleId"/>
                            <div class="form-group">
                                <div class="col-sm-5">
                                    <button type="submit"  class="btn btn-white btn-warning btn-bold"  id="btnUpdateNew"/>Cập nhật
                                    <i class="ace-icon fa fa-arrow-right icon-on-right bigger-110"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script >
    $('#btnUpdateNew').click(function (e) {
        e.preventDefault();
        var data = {};
        var formData = $('#formSubmit').serializeArray();
        var dataList = [];
        $.each(formData, function (i, v) {
            if (v.name == 'permissionId' ) {
                dataList.push(v.value);
            }
        });
        data['permissionId'] = dataList;
        updateNew(data);
    });
    function updateNew(data) {
        console.log('${APIurl}');
        $.ajax({
            url: '${APIurl}',
            type: 'PUT',
            contentType: 'application/json',
            data: JSON.stringify(data),
            dataType: 'json',
            success: function () {
                window.location.href = "${urlList}?&message=update_success";
            },
            error: function () {
                window.location.href = "${urlList}?message=error_system";
            }
        });
    }
</script>
</body>
</html>
