<%@include file="/common/taglib.jsp"%>
<%@ page import="com.vnpost.demo.util.SecurityUtils" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<c:url var="urlList" value="/admin/user/list"/>
<c:url var="APIurl" value="/admin/api/user/delete"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Danh sách admin</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"/>
<%--    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>--%>
<%--    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>--%>
<%--    		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>--%>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
    <script src="<c:url value='/common/paging/jquery.twbsPagination.js'/> "></script>
    <link href="<c:url value='/common/admin/fontawesome-free/css/all.min.css'/>" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="<c:url value='/common/admin/sweetalert/sweetalert2.min.css'/> ">
    <script src="<c:url value='/common/admin/sweetalert/sweetalert2.min.js'/> "></script>

    <!-- Custom styles for this template-->
    <link href="<c:url value='/css/admin/sb-admin.css'/>" rel="stylesheet">

</head>

<body>
    <%@include file="/common/admin/header.jsp"%>

    <div id="wrapper">
        <%@include file="/common/admin/menu.jsp"%>
        <div class="container-fluid" style="width: 100%">
            <form action="#" id="formSubmit" method="get" >
                <div class="main-content-inner">
                    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
                        <ul class="breadcrumb">
                            <li>
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="<c:url value="/admin"/>"><i class="ace-icon fa fa-home home-icon"></i><b>Trang chủ</b></a>
                                    </li>
                                    <li class="breadcrumb-item active"><b>Danh sách admin</b></li>
                                </ol>
                            </li>
                        </ul>
                        <!-- /.breadcrumb -->
                    </div>
                    <div class="page-content">
                        <div class="row" style="padding-left: 50px">
                            <div class="col-xs-12">
                                <c:if test="${not empty message}">
                                    <div class="alert alert-${alert}">
                                            ${message}
                                    </div>
                                </c:if>
                                <hr>
                                <div class="container widget-box table-filter row" >
                                    <div class="table-btn-controls" style="width: 100%">
                                        <div class="pull-right tableTools-container">
                                            <div class="dt-buttons btn-overlap btn-group" >
                                                <c:forEach var="item" items="${roles}">
                                                    <c:if test="${item == 'ADMIN'}">
                                                        <a flag="info"
                                                           class="dt-button buttons-colvis btn btn-white btn-primary btn-bold" data-toggle="tooltip"
                                                           title='Thêm thành viên' href='/admin/user/create-user'>
                                                    <span>
                                                        <i class="fa fa-plus-circle bigger-110 purple"></i>
                                                    </span>
                                                        </a>
                                                    </c:if>
                                                    <c:if test="${item == 'SUPER'}">
                                                        <button id="btnDelete" type="button" onclick="warningBeforeDelete()"
                                                                class="dt-button buttons-html5 btn btn-white btn-primary btn-bold" data-toggle="tooltip" title='Xóa'>
																<span>
																	<i class="fa fa-trash" aria-hidden="true"></i>
																</span>
                                                        </button>
                                                    </c:if>
                                                </c:forEach>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                </div>
                                <div class="row container">
                                    <div class="col-xs-12">
                                        <div class="table-responsive">
                                            <table class="table table-bordered">
                                                <thead>
                                                <tr>
                                                    <th></th>
                                                    <th>Quyền hạn</th>
                                                    <th>Avatar</th>
                                                    <th>Họ và tên</th>
                                                    <th>Email</th>
                                                    <th>Phone</th>
                                                    <th>Giới tính</th>
                                                    <th>Địa chỉ</th>
                                                    <th>Ngày tạo</th>
                                                    <th>Thao tác</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <c:forEach var="item" items="${users}">
                                                    <tr>
                                                        <td><input type="checkbox" id="checkbox_${item.id}" value="${item.id}"></td>
                                                        <td>${item.roleName}</td>
                                                        <td>
                                                            <img src="${item.avatar}" style="height: 90px; width: 80px">
                                                        </td>
                                                        <td>${item.fullName}</td>
                                                        <td>${item.email}</td>
                                                        <td>${item.phone}</td>
                                                        <td>${item.gender}</td>
                                                        <td>${item.address}</td>
                                                        <td>
                                                            <fmt:formatDate type = "both"
                                                                            dateStyle = "short" timeStyle = "short"
                                                                            pattern="dd-M-yyyy" value = "${item.createdDate}" />
                                                        </td>
                                                        <td>
                                                            <div class="dt-buttons btn-overlap btn-group">
                                                                <c:forEach var="itemRole" items="${roles}">
                                                                    <c:if test="${itemRole == 'SUPER'}">
                                                                        <a flag="info"
                                                                           class="dt-button buttons-colvis btn btn-white btn-primary btn-bold" data-toggle="tooltip"
                                                                           title='Cập nhật thông tin' href='<c:url value="/admin/user/edit-user/${item.id}"/>'>
                                                                        <span>
                                                                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                                        </span>
                                                                        </a>
                                                                    </c:if>
                                                                </c:forEach>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </c:forEach>
                                                </tbody>
                                            </table>
                                            <ul class="pagination" id="pagination"></ul>
                                            <input type="hidden" value="" id="page" name="page"/>
                                            <input type="hidden" value="" id="limit" name="limit"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
<!-- /.main-content -->
<script>
    <%--var totalPages = ${model.totalPage};--%>
    <%--var currentPage = ${model.page};--%>
    <%--$(function () {--%>
    <%--    window.pagObj = $('#pagination').twbsPagination({--%>
    <%--        totalPages: totalPages,--%>
    <%--        visiblePages: 10,--%>
    <%--        startPage: currentPage,--%>
    <%--        onPageClick: function (event, page) {--%>
    <%--        	if (currentPage != page) {--%>
    <%--        		$('#limit').val(2);--%>
    <%--				$('#page').val(page);--%>
    <%--				$('#formSubmit').submit();--%>
    <%--			}--%>
    <%--        }--%>
    <%--    });--%>
    <%--});--%>
    function warningBeforeDelete() {
        swal({
            title: "Xác nhận xóa",
            text: "Bạn có chắc chắn muốn xóa hay không",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Xác nhận",
            cancelButtonText: "Hủy bỏ",
        }).then(function(isConfirm) {
            if (isConfirm.value) {
                var ids = $('tbody input[type=checkbox]:checked').map(function () {
                    return $(this).val();
                }).get();
                console.log(ids);
                deleteNew(ids);
            }

        });
    }
    function deleteNew(data) {
        $.ajax({
            url: '${APIurl}',
            type: 'DELETE',
            contentType: 'application/json',
            data: JSON.stringify(data),
            success: function () {
                window.location.href = "${urlList}?message=delete_success";
            },
            error: function () {
                window.location.href = "${urlList}?message=error_system";
            }
        });
    }
</script>
</body>
</html>
