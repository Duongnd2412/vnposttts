<%@ include file="/common/taglib.jsp"%>
<%@ page pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>VnPost | Đăng nhập</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">

    <!-- Custom styles for this template -->
    <link href="<c:url value='/css/web/tin-tuc.css'/>" rel="stylesheet">
    <!-- icon -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


</head>

<body>

<!-- Navigation -->
<%@ include file="/common/web/header1.jsp"%>
<!------------------------------------>

<div class="container">
    <div class="card card-login mx-auto mt-5"  style="width: 600px">
        <div class="card-header">Login</div>
        <div class="card-body">
            <c:if test="${param.incorrectAccount != null}">
                <div class="alert alert-danger">
                    Đăng nhập thất bại!
                </div>
            </c:if>
            <form action="/j_spring_security_check" method="post">
                <div class="form-group">
                    <div class="form-label-group">
                        <input type="text" class="form-control" name="j_username" placeholder="Email" required="required">
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-label-group">
                        <input type="password" id="inputPassword" name="j_password" class="form-control" placeholder="Password">
                    </div>
                </div>
                <div class="form-group">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" value="remember-me">
                            Remember Password
                        </label>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary btn-block">Login</button>
            </form>
        </div>
    </div>
</div>
<!-- /.container -->

<!-- Footer -->
<%@ include file="/common/web/footer.jsp"%>

<!-- Bootstrap core JavaScript -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>

</body>

</html>
