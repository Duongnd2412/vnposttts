<%@ page pageEncoding="UTF-8" %>

    <!-- Sidebar -->
    <ul class="sidebar navbar-nav">

            <li class="nav-item active">
                <a class="nav-link" href="<c:url value="/admin"/>">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>Dashboard</span>
                </a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown2" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-fw fa-folder"></i>
                    <span>Quản lý</span>
                </a>
                <div class="dropdown-menu" aria-labelledby="pagesDropdown">
                    <c:forEach items="${roles}" var="item">
                        <c:if test="${item == 'BCCP'}">
                            <a class="dropdown-item" href="#">Bưu chính chuyển phát</a>
                        </c:if>
                        <c:if test="${item == 'TCBC'}">
                            <a class="dropdown-item" href="#">Tài chính bưu chính</a>
                        </c:if>
                        <c:if test="${item == 'PPTT'}">
                            <a class="dropdown-item" href="#">Phân phối truyền thông</a>
                        </c:if>
                        <c:if test="${item == 'TT'}">
                                <a class="dropdown-item dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Tin tức</a>
                                <ul class="dropdown-menu">
                                    <a href="<c:url value='/admin/new/list?page=1&limit=5'/>"><li>&nbsp;Bài viết</li></a>
                                    <a href="<c:url value='/admin/category/list?page=1&limit=10'/>"><li>&nbsp;Thể loại</li></a>
                                    <a href="<c:url value='/admin/group-category/list?page=1&limit=10'/>"><li>&nbsp;Nhóm thể loại</li></a>
                                </ul>
                        </c:if>
                    </c:forEach>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown1" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-fw fa-table"></i>
                    <span>Member</span>
                </a>
                <div class="dropdown-menu" aria-labelledby="pagesDropdown">
                    <a class="dropdown-item" href="<c:url value="/admin/user/list"/>">Danh sách Admin</a>
                    <c:forEach items="${roles}" var="item">
                        <c:if test="${item == 'SUPER'}">
                            <a class="dropdown-item" href="<c:url value='/admin/user/role'/>">Quyền hạn</a>
                        </c:if>
                </c:forEach>
                </div>
            </li>
    </ul>

    <!-- /.content-wrapper -->
    <!-- Sticky Footer -->

<!-- /#wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>
