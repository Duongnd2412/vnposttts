<%@ page pageEncoding="UTF-8" %>
<footer style="padding-top: 20px;">
    <div style="background-color: #fcb71e;">
        <div class="row" style="padding-left: 200px">
            <div class="col-md-4 col-sm-12">
                <b>Bưu chính chuyển phát</b>
                <ul class="list-group-itemft">
                    <li class="itemft"><a class="list-group-itemft" href="#">Bưu chính chuyển phát trong nước</a></li>
                    <li class="itemft"><a class="list-group-itemft" href="#">Bưu chính chuyển phát Quốc tế</a></li>
                </ul>
            </div>
            <div class="col-md-4 col-sm-12">
                <b>Tài chính bưu chính</b>
                <ul class="list-group-itemft">
                    <li class="itemft"><a class="list-group-itemft" href="#" >Bảo hiểm phi nhân thọ PTI</a></li>
                    <li class="itemft"><a href="#" class="list-group-itemft">Thu hộ-Chi hộ</a></li>
                    <li class="itemft"><a href="#" class="list-group-itemft">Đại lý bảo hiểm nhân thọ(Dai-ichi)</a></li>
                    <li class="itemft"><a href="#" class="list-group-itemft">Đại lý ngân hàng</a></li>
                    <li class="itemft"><a href="#" class="list-group-itemft">Dịch vụ chuyển tiền trong nước</a></li>
                </ul>

            </div>
            <div class="col-md-4 col-sm-12">
                <b>Phân phối-Truyền thông</b>
                <ul class="list-group-itemft" >
                    <li class="itemft"><a href="#" class="list-group-itemft">Sàn thương mại điện tử POSTMART</a></li>
                    <li class="itemft"><a href="#" class="list-group-itemft">Truyền thông, quảng cáo</a></li>
                    <li class="itemft"><a href="#" class="list-group-itemft">Phân phối xuất bản ấn phẩm</a></li>
                    <li class="itemft"><a href="#" class="list-group-itemft">Dịch vụ viễn thông-CNTT</a></li>
                    <li class="itemft"><a href="#" class="list-group-itemft">Dịch vụ phân phối hàng hóa</a></li>
                </ul>

            </div>
        </div>

    </div>
    <div style="padding-left: 200px; background-color: #144e8c;">
        <div class="col-md-5 col-sm-12">
            <b style="color: white;font-size: 10px;">TỔNG CÔNG TY BƯU ĐIỆN VIỆT NAM - VIETNAM POST</b><br>
            <a style="color: white;font-size: 10px;">Địa chỉ: Số 05 đường Phạm Hùng - Mỹ Đình 2 - Nam Từ Liêm - Hà Nội - Việt Nam</a><br>
            <a style="color: white;font-size: 10px;">Ghi rõ nguồn "www.vnpost.vn" khi phát hành lại thông tin từ website này</a>
        </div>
        <div class="col"></div>
    </div>
    <!-- /.container -->
</footer>
