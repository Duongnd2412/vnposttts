<%@ page pageEncoding="UTF-8" %>
<div>
    <div class="head1" >
        <div class="container">
            <div class="hotline">
                <a class="nav-link"><i class="fa fa-phone"></i>Đường dây nóng hỗ trợ   1900 54 54 81</a>
            </div>
            <div class="nav-link">
                <ul class="list1">
                    <li >
                        <a class="fontC" href="<c:url value="/trang-chu"/>">Giới thiệu</a>
                    </li>
                    <li class="rows2">
                        <a class="fontC" href="#">Hỏi đáp</a>
                    </li>
                    <li class="rows2">
                        <a class="fontC" href="#">Liên hệ</a>
                    </li>
                    <li class="rows2">
                        <a class="fontC" href="<c:url value="/dang-nhap"/> ">Login</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid row">
        <div class="col-md-4 col-sm-12 img-header" >
            <a class="navbar-brand" href="<c:url value="/trang-chu"/>" ><img class="img-header" src="<c:url value='/image/vnpost.png'/>"></a>
        </div>
        <div class="col-md-8 col-sm-12">
            <ul class="row" style="padding: 10px 20%">
                <li class="list-header col-md-4 ">
                    <div class="row">
                        <div class="col-left col-md-2">
                            <img src="http://www.vnpost.vn/Portals/_default/Skins/VNPost.Skins.FrontEnd/img/search-price.png" style="padding-top: 17px;">
                        </div>
                        <div class="col-left col-md-10">
                            <a class="nav-link" href="#"><b>Tra cước</b><br>DỊCH VỤ</a>
                        </div>
                    </div>
                </li>
                <li class="list-header col-md-4">
                    <div class="row">
                        <div class="col-left col-md-2">
                            <img src="http://www.vnpost.vn/Portals/_default/Skins/VNPost.Skins.FrontEnd/img/mess.png" style="padding-top: 17px;" >
                        </div>
                        <div class="col-left col-md-10">
                            <a class="nav-link" href="#"><b>Đánh giá&</b><br>KHIẾU NẠI</a>
                        </div>
                    </div>
                </li>
                <li class="list-header col-md-4 ">
                    <div class="row">
                        <div class="col-left col-md-2">
                            <img src="http://www.vnpost.vn/Portals/_default/Skins/VNPost.Skins.FrontEnd/img/recruitment.png" style="padding-top: 17px;">
                        </div>
                        <div class="col-left col-md-10">
                            <a class="nav-link" href="#"><b>Tin</b><br>TUYỂN DỤNG</a>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <nav class="navbar navbar-expand-lg navbar-light bg-light nav-header">
        <a class="nav-link dropdown-toggle padding" href="#" id="navbarDropdown" role="button" data-toggle="dropdown">
            Tra cứu - Định vị
        </a>
        <div class="dropdown-menu" style="background-color: #ececec">
            <a class="dropdown-item" href="#" ><i class="fa fa-map-marker"></i> | Định vị bưu gửi</a>
            <a class="dropdown-item" href="#" ><i class="far fa-money-bill-alt"></i> | Định vị chuyển tiền</a>
            <a class="dropdown-item" href="#" ><i class="fa fa-map"></i> | Mạng lưới bưu cục</a>
            <a class="dropdown-item" href="#" >Tra cứu hàng cấm</a>
            <a class="dropdown-item" href="#" >Tra cứu kỳ cước KHL</a>
            <a class="dropdown-item" href="#" ><i class="fa fa-qrcode"></i> | Mã địa chỉ bưu chính</a>
        </div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse padding" id="navbarSupportedContent" style="background-color: #fcb71e">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link font" href="#">Bưu chính chuyển phát</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link font" href="#">Tài chính bưu chính</a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link font" href="#">Phân phối - truyền thông</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link font" href="<c:url value="/tin-tuc"/> ">Tin tức</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link font" href="#">Email</a>
                </li>
            </ul>
        </div>
    </nav>
</div>

